#version 150

/*
** Contrast, saturation, brightness
** Borrowed from TGM's shader pack
** http://irrlicht.sourceforge.net/phpBB2/viewtopic.php?t=21057
*/


uniform sampler2DRect bgl_RenderedTexture;
uniform sampler2DRect ch1;

uniform float contrast;
uniform float saturation;
uniform float brightness;

in vec2 texCoordVarying;
out vec4 outputColor;

vec3 ContrastSaturationBrightness(vec3 color, float con, float sat, float brt)
{
    // Increase or decrease theese values to adjust r, g and b color channels seperately
    const float AvgLumR = 0.5;
    const float AvgLumG = 0.5;
    const float AvgLumB = 0.5;
    
    const vec3 LumCoeff = vec3(0.2125, 0.7154, 0.0721);
    
    vec3 AvgLumin = vec3(AvgLumR, AvgLumG, AvgLumB);
    vec3 brtColor = color * brt;
    vec3 intensity = vec3(dot(brtColor, LumCoeff));
    vec3 satColor = mix(intensity, brtColor, sat);
    vec3 conColor = mix(AvgLumin, satColor, con);
    return conColor;
}


void main()
{

    vec3 layer_1 = vec3(texture( ch1, texCoordVarying ));
    
    outputColor = vec4(ContrastSaturationBrightness(layer_1, contrast, saturation, brightness),1);
}
