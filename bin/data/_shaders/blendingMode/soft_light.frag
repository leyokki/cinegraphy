#version 150

/*
*/

uniform sampler2DRect bgl_RenderedTexture;
uniform sampler2DRect ch1;
uniform sampler2DRect ch2;

uniform float fade_ch1;
uniform float fade_ch2;

in vec2 texCoordVarying;
out vec4 outputColor;

void main()
{

    vec3 layer_1 = vec3(texture( ch1, texCoordVarying )) * fade_ch1;
    vec3 layer_2 = vec3(texture( ch2, texCoordVarying )) * fade_ch2;
    
    vec3 result;
    
    for (int i = 0; i < 3; ++i) {
        float a = layer_1[i];
        float b = layer_2[i];
        result[i] = b < .5 ? (2 * a * b + a * a * (1 - 2 * b)) : (sqrt(a) * (2 * b - 1) + (2 * a) * (1 - b));
    }
    
    outputColor = vec4(result ,1);
}
