#version 150

/*
*/

uniform sampler2DRect bgl_RenderedTexture;
uniform sampler2DRect ch1;
uniform sampler2DRect ch2;

uniform float fade_ch1;
uniform float fade_ch2;

in vec2 texCoordVarying;
out vec4 outputColor;

void main()
{

    vec3 layer_1 = vec3(texture( ch1, texCoordVarying )) * fade_ch1;
    vec3 layer_2 = vec3(texture( ch2, texCoordVarying )) * fade_ch2;
        
    outputColor = vec4(max(layer_1, layer_2) ,1);
}
