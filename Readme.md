# Readme

```
WARNING:: This software is still a wip, it might have a lot of errors and not work at all. I just wanted to create a public archive for this diy-software I am making.
```

Cinegraphy is an open source vjing software:: a tool to edit and compose videos, and to record them, in real time. It is made with [openFrameworks](https://openframeworks.cc/), ofxHapPlayer, and a bunch of other addons. It has no GUI for now.
I am using this software to play live in concerts, [here](https://www.youtube.com/watch?v=dSRHZCmiU5Y) for example, and to manage craftily videos, like [this one](https://www.youtube.com/watch?v=0qqXraSv_TY).

# Install

To make it work, you will need the following::

### Addons

First, you need to add these addons. You can find them easily on github or on the openframeworks website.

```sh
 ofxHapPlayer
 ofxFFmpegRecorder
 ofxFastFboReader
 ofxMidi
 ofxOpenCv
 ofxTextureRecorder
 ofxXmlSettings
 ofxGui
 ofxMSAInteractiveObject
```


### Setup

Then, you have to place your elements in the following folders of the project

```python
 /bin
      /data
            /_xml/             # the config xml file for the console,
                               # you can see the referene in this project
                 /keymap/      # the config for the midi and shortcuts
                 
            /_import/videos    # the videos, into a folder referenced by the config xml
                    /images    # the images... 
                    /sound     # the sounds [not yet configured]
            /_export          # folder used for the exported videos
```


