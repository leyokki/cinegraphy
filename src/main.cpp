#include "ofMain.h"
#include "ofApp.h"
#include "ofAppGLFWWindow.h"

#include "utils/utilities.hpp"

#define WINDOWED

const int GLOBAL_W = 540;
const int GLOBAL_H = 540;

int main() {
	ofGLFWWindowSettings settings;
    
#ifdef WINDOWED
	settings.setSize(GLOBAL_W,GLOBAL_H);
	settings.windowMode = OF_WINDOW;
#else
	settings.windowMode = OF_FULLSCREEN;
#endif
	settings.setGLVersion(3, 2);
    
    shared_ptr<ofAppBaseWindow> mainWindow = ofCreateWindow(settings);

    shared_ptr<ofApp> mainApp(new ofApp);
    
    ofRunApp(mainWindow, mainApp);
    ofRunMainLoop();
    
}
