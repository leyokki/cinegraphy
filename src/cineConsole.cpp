//
//  cineConsole.cpp
//  midiMapper
//
//  Created by lky on 28/01/2020.
//

#include "cineConsole.hpp"

cineConsole::cineConsole(){
}

cineConsole::cineConsole(string name) {
    setup(m_name);
}

cineConsole::~cineConsole(){
    ofLog() << "cineConsole " << m_name << " destroyed." << std::endl;
}

// Clone starts from scratch - should be updated later
cineConsole::cineConsole(cineConsole const& other):
m_name(other.m_name)
{
    setup(m_name);
}

void cineConsole::setup(string path) {
    tmp_threshMod = 0;
    m_blend_mode = 0;
    m_actual_channel = 0;
    
    load_xml(path);
    
    ofTrueTypeFont::setGlobalDpi(72);
    log_font.load("fonts/Futura.ttc", 14, true, true);
    log_font.setLineHeight(18.0f);
    log_font.setLetterSpacing(1.037);
    
    second_contrast = 1.0;
    second_saturation = 1.0;
    second_brightness = 1.0;
}


void cineConsole::load_xml(string path) {
    ofxXmlSettings xml_presets;
    
    if (xml_presets.loadFile("_xml/"+path+".xml")) {
        ofLog() << "xml loaded!" << '\n';
    } else {
        ofLog() << "unable to load " + path + " check _xml/ folder" << '\n';
    }
    
    m_scale = xml_presets.getValue("GLOBAL_SCALE", 1.0); // don't forget the 1.0, it is a double
    
    m_global_w = xml_presets.getValue("GLOBAL_W", 1440);
    m_global_h = xml_presets.getValue("GLOBAL_H", 1080);
    
    m_x_pos = xml_presets.getValue("GLOBAL_X_POS", 0.0);
    m_y_pos = xml_presets.getValue("GLOBAL_Y_POS", 0.0);
    
    m_saturation = xml_presets.getValue("SATURATION",1.0);
    m_contrast = xml_presets.getValue("CONTRAST",1.0);
    m_fade_value = xml_presets.getValue("FADE",1.0);
    
//    m_difference_mode_activated = xml_presets.getValue("BLENDMDE",0);
    
    m_grain_amount = xml_presets.getValue("GRAIN_AMOUNT",.04);//04; // old: .12
    m_grain_size = xml_presets.getValue("GRAIN_SIZE",.27);//27; // old:.9
    m_is_grain_colored = xml_presets.getValue("GRAIN_COLORED",0.0);
    
    int number_of_channels = xml_presets.getNumTags("CHANNEL") > 0 ? xml_presets.getNumTags("CHANNEL") : 1;
    
    for (int i = 0; i < number_of_channels; ++i) {
        xml_presets.pushTag("CHANNEL",i);
        string type = xml_presets.getValue("TYPE", "cam");
        string path = xml_presets.getValue("PATH", "0");
        int is_active = xml_presets.getValue("IS_ACTIVE", 0);
        m_blend_modes.push_back(1);
        add_channel(type, m_global_w, m_global_h, path);
        m_channels[i].activate(is_active);
        xml_presets.popTag();
    }
    ofLog() << "[XML-PRESET] " << m_global_w << " "  << m_global_h << " " << m_x_pos << " "  << m_y_pos << " " << m_scale << '\n';
    ofLog() << "[XML-PRESET] " << number_of_channels << " channels " << '\n';
    ofLog() << "Console set up channels " << '\n';
}

void cineConsole::add_channel(string type, int w, int h, string path) {
    m_channels.push_back(cineChannel(type,w,h,path));
}

void cineConsole::update() {
    for (int i = 0; i < m_channels.size(); ++i) {
        m_channels[i].is_active() ? m_channels[i].update() : void();
    }
}

void cineConsole::get_lines() {
    lines = m_channels[0].get_lines(m_channels[0].get_fade()+m_contrast,m_channels[1].get_fade()+m_contrast);
}

void cineConsole::draw(){
}

void cineConsole::draw_three_screens(const ofFbo reference,const int w, const int h) {
    // Draw three screens horizontally
    reference.draw(w,h);
    reference.draw(w + m_global_w/2,h);
    reference.draw(w + m_global_w,h);
}

ofFbo cineConsole::get_fbo() {
    return mix_multiple_channels();
}

void cineConsole::set_x_pos(const double value) {
    m_x_pos = value;
}
void cineConsole::set_y_pos(const double value) {
    m_y_pos = value;
}
void cineConsole::set_scale(const double value) {
    m_scale = value;
}

void cineConsole::set_fade(const double value) {
    m_fade_value = value;
}

void cineConsole::set_thresh(double value) {
    m_thresh = value;
}

void cineConsole::switch_thresh() {
    tmp_threshMod = tmp_threshMod == 1 ? 0 : 1;
}

void cineConsole::set_brightness(const double value) {
    m_brightness = value;
}
void cineConsole::set_contrast(const double value) {
    m_contrast = value;
}
void cineConsole::set_saturation(const double value) {
    m_saturation = value;
}

/*** CHANNEL SPECIFIC *****/

void cineConsole::activate_channel(const int channel, const bool value) {
    if ((channel >= 0) && (channel < m_channels.size())) {
        m_channels[channel].activate(value);
        m_channels[channel].set_fade_value(value);
    } else {
        cerr << "Invalid channel number." << endl;
    }
}

void cineConsole::update_channel_fade(const int channel, const double value) {
    if ((channel >= 0) && (channel < m_channels.size())) {
        m_channels[channel].set_fade_value(value);
    } else {
        cerr << "Invalid channel number." << endl;
    }
}

void cineConsole::update_channel_reversed_fade(const int channel, const double value) {
    if ((channel >= 0) && (channel < m_channels.size())) {
        m_channels[channel].set_reversed_fade(value);
    } else {
        cerr << "Invalid channel number." << endl;
    }
}

void cineConsole::update_channel_image(const int channel, const int value) {
    ofLogVerbose();
    ofLog() << "Updating " << channel << " to " << value << "value;" << std::endl;
    if ((channel >= 0) && (channel < m_channels.size())) {
        m_channels[channel].update_actual_image(value);
    } else {
        cerr << "Invalid channel number." << std::endl;
    }
}

void cineConsole::update_channel_speed_or_frame(const int channel, const double value) {
    if ((channel >= 0) && (channel < m_channels.size())) {
        m_channels[channel].update_speed_or_frame(value);
    } else {
        cerr << "Invalid channel number." << std::endl;
    }
}

void cineConsole::reset_channel(int channel) {
    if ((channel >= 0) && (channel < m_channels.size())) {
        m_channels[channel].reset();
    } else {
        cerr << "Invalid channel number." << std::endl;
    }
}

void cineConsole::reset_channel_image(int channel) {
    if ((channel >= 0) && (channel < m_channels.size())) {
        m_channels[channel].update_actual_image(0);
    } else {
        cerr << "Invalid channel number." << std::endl;
    }
}

void cineConsole::increment_channel_image(int channel) {
    if ((channel >= 0) && (channel < m_channels.size())) {
        m_channels[channel].increment_image();
    } else {
        cerr << "Invalid channel number." << std::endl;
    }
}

void cineConsole::decrement_channel_image(const int channel) {
    if ((channel >= 0) && (channel < m_channels.size())) {
        m_channels[channel].decrement_image();
    } else {
        cerr << "Invalid channel number." << std::endl;
    }
}

double cineConsole::get_global_w() {
    return m_global_w;
}

double cineConsole::get_global_h(){
    return m_global_h;
}

void cineConsole::switch_channel_mirror(const int channel, const int axis_x) {
    if ((channel >= 0) && (channel < m_channels.size())) {
        if (axis_x == 1) {
            m_channels[channel].switch_mirror_x();
        } else {
            m_channels[channel].switch_mirror_y();
        }
    } else {
       cerr << "Invalid channel number." << std::endl;
   }
}

void cineConsole::increment_blend_mode() {
    m_blend_mode = (m_blend_mode < 12) ? ++m_blend_mode : m_blend_mode;
    ofLog() << "Increment" << m_blend_mode << endl;
}

void cineConsole::decrement_blend_mode() {
    m_blend_mode = (m_blend_mode > 0) ? --m_blend_mode : m_blend_mode;
    ofLog() << "Decrement" << m_blend_mode << endl;
}

void cineConsole::set_blend_modes(int channel, int value) {
    if ((channel >= 0) && channel < m_blend_modes.size()) {
        m_blend_modes[channel] = value < 12 ? value : value >= 0 ? 0 : value;
    }
    ofLog() << "set channel " << channel << " blend_mode to" << value << endl;
}

void cineConsole::increment_blend_modes(int channel) {
    m_blend_modes[channel] = (m_blend_modes[channel] < 12) ? ++m_blend_modes[channel] : m_blend_modes[channel];
    ofLog() << "increment" << m_blend_modes[channel] << endl;
}

void cineConsole::decrement_blend_modes(int channel){
    m_blend_modes[channel] = (m_blend_modes[channel] > 0) ? --m_blend_modes[channel] : m_blend_modes[channel];
    ofLog() << "decrement" << m_blend_modes[channel] << endl;
}

void cineConsole::increment_local_h(int channel) {
    if ((channel >= 0) && channel < m_channels.size()) {
        m_channels[channel].increment_local_h();
    }
}
void cineConsole::decrement_local_h(int channel){
    if ((channel >= 0) && channel < m_channels.size()) {
        m_channels[channel].decrement_local_h();
    }
}
void cineConsole::increment_local_w(int channel){
    if ((channel >= 0) && channel < m_channels.size()) {
        m_channels[channel].increment_local_w();
    }
}
void cineConsole::decrement_local_w(int channel){
    if ((channel >= 0) && channel < m_channels.size()) {
            m_channels[channel].decrement_local_w();
    }
}


void cineConsole::get_blend_mode_channel(int channel) {
    if ((channel >= 0) && channel < m_channels.size()) {
        ofLog() << " blend mode channel " << channel << " :: " << m_blend_modes[channel] << endl;
    }
}

void cineConsole::set_second_sat(float val) {
    second_saturation = val;
}
void cineConsole::set_second_bright(float val) {
    second_brightness = val;
}
void cineConsole::set_second_cont(float val) {
    second_contrast = val;
}

void cineConsole::switch_channel() {
    if (m_channels.size() > 1) {
        m_actual_channel = (m_actual_channel == 0) ? 1 : 0;
    }
}
