//
//  cineChannel.hpp
//  midiMapper
//
//  Created by lky on 11/12/2019.
//

#ifndef cineChannel_hpp
#define cineChannel_hpp

#include <stdio.h>

#include "ofMain.h"

#include "modules/cineModule.hpp"
#include "modules/components/cineImage/cineImage.hpp"
#include "modules/components/cineVideo/cineVideo.hpp"
#include "modules/components/cineCam/cineCam.hpp"

#include "utilities.hpp"

enum videoMode {
    standard = 0x01,
    still = 0x02
};

class cineChannel: public ofBaseApp {
public:
   
    cineChannel();
    cineChannel(string cine_type, int w, int h, string path);
    
    void set_fbo(int w, int h);
    void reset();
    
    void update();
    void update(const double& frame_or_speed, const int& video_number);
    void update_fbo();
    
    void draw(float x, float y);
    void draw(double scale = 1.0);
    void draw_reversed(float x = 0, float y = 0);
    
    vector<ofPolyline> get_lines(float max_th, float min_th);

    void add_draw(float x, float y);

    void activate(bool value=true);
    
    void set_fade_value(double value);
    void set_reversed_fade(double value);
    void set_video_mode(bool mode);

    void set_mirror_x(int value);
    void set_mirror_y(int value);
    
    void switch_mirror_x();
    void switch_mirror_y();
    
    void increment_image();
    void decrement_image();
    
    void increment_local_w();
    void decrement_local_w();
    void increment_local_h();
    void decrement_local_h();
    
    void update_actual_image(const int& value);
    void update_speed_or_frame(const double& value);
    
    int is_active();
    int get_actual_image();
    
    double get_fade();
    double get_reversed_fade();

    ofFbo get_fbo(float scale_x_axis=1, float scale_y_axis=1);
    ofFbo get_motion_fbo(float scale_x_axis=1, float scale_y_axis=1);
    ofFbo get_fbo(float x_translate, float y_translate, float scale_x_axis, float scale_y_axis);
    ofFbo fbo_reversed(float x = 0, float y = 0);
    
    ofFbo get_local_fbo();
    
    ofTexture* getTexture();
    
    const int get_width();
    const int get_heigth();
    
private:
    bool m_enabled;
    int m_local_w, m_local_h;
    int m_mirror_x, m_mirror_y;
    
    ofFbo m_fbo;
    cineModule* m_module;
    double m_fade;
    double m_reversed_fade;
    
    videoMode m_mode;
    
    int m_x_motion, m_y_motion;
    int m_autoflip_x, m_autoflip_y;
    int m_activate_mirror_images;
    ofBlendMode m_inner_blend_mode;
    
    double m_x, m_y;
    int change_direction;
};

#endif /* cineChannel_hpp */
