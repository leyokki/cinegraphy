//
//  cineChannel.cpp
//  midiMapper
//
//  Created by lky on 11/12/2019.
//


#include "utilities.hpp"
#include "cineChannel.hpp"

// static enum not permitted?
enum cineTypes {
    video,
    cam,
    image
};

static std::map<std::string, cineTypes> cineTypes_map_to_string;

void initialize_cinetypes_map_value() {
    cineTypes_map_to_string["video"] = video;
    cineTypes_map_to_string["cam"] = cam;
    cineTypes_map_to_string["image"] = image;
}

cineChannel::cineChannel(){
    m_enabled = false;
}

cineChannel::cineChannel(string cine_type, const int w, const int h, string path){
    set_fbo(w, h);
    initialize_cinetypes_map_value();
    
    cout << "init new cineChannel with " << cine_type << endl;
    
    switch (cineTypes_map_to_string[cine_type]) {
        case video:
            m_module = new cineVideo(path);
            break;
        case cam:
            m_module = new cineCam(w,h,std::stoi(path));
            break;
        case image:
            m_module = new cineImage(path);
            break;
        default:
            ofLogError("Non normalized type. \n");
    }
    m_mirror_x = 1; m_mirror_y = 1;
    m_mode = standard;
    m_enabled = true;
    
    m_inner_blend_mode = OF_BLENDMODE_ADD;
    m_autoflip_x = -1;
    m_autoflip_y = 1;
    m_activate_mirror_images = 1;
    m_reversed_fade = 0;
    
    change_direction = 1;
    
    m_x = 0;
    m_y = 0;
    
    cout << "local values :: " << m_local_h << " : " << m_local_w << endl;
}

const int cineChannel::get_width() {
    return m_local_w;
}
const int cineChannel::get_heigth() {
    return m_local_h;
}


void cineChannel::set_fbo(const int w, const int h) {
    m_fbo = cineUtils::utils_init_fbo(w, h);

    m_local_h = h;
    m_local_w = w;
}


void cineChannel::update(){
    (m_mode == still) ? m_module->update_still() : m_module->update();
    update_fbo();
}

void cineChannel::update(const double& frame_or_speed,const int& video_number)  {
    (m_mode == still) ? m_module->update_still(frame_or_speed,video_number) : m_module->update(frame_or_speed,video_number);
    update_fbo();
}

void cineChannel::set_video_mode(bool mode) {
    m_mode = mode ? standard : still;
}

void cineChannel::draw(double scale) {
    m_module->draw(scale);
}

void cineChannel::draw(float x, float y) {
    m_module->draw(x, y);
}

void cineChannel::increment_image(){
    m_module->increment_image();
}

void cineChannel::decrement_image(){
    m_module->decrement_image();
}

void cineChannel::update_actual_image(const int& value) {
    m_module->update_image_selected(value);
}

void cineChannel::update_speed_or_frame(const double& value) {
    m_mode == still ? m_module->update_frame(value) : m_module->update_speed(value);
}

int cineChannel::get_actual_image() {
    return m_module->get_image_selected();
}

void cineChannel::set_mirror_x(const int value) {
    m_mirror_x = value;
}

void cineChannel::set_mirror_y(const int value) {
    m_mirror_y = value;
}

void cineChannel::switch_mirror_x() {
    m_mirror_x = (m_mirror_x == 1) ? -1 : 1;
    cout << "m_mirror_x - " << m_mirror_x << endl;
}

void cineChannel::switch_mirror_y() {
    m_mirror_y = (m_mirror_y == 1) ? -1 : 1;
}

void cineChannel::reset(){
    m_module->reset();
}

void cineChannel::update_fbo(){
    m_fbo.begin();
    ofPushMatrix();
    
    cineUtils::default_background(m_local_w, m_local_h);
    
    m_module->draw();

    ofPopMatrix();
    m_fbo.end();
}

ofFbo cineChannel::get_local_fbo() {
    return m_fbo;
}

ofFbo cineChannel::get_fbo(const float x_translate, const float y_translate, const float scale_x_axis, const float scale_y_axis) {
    ofFbo new_fbo = m_fbo;
    
    new_fbo.begin();
    ofPushMatrix();
    
    cineUtils::default_background(m_local_w, m_local_h);
    
    ofTranslate(m_x + (x_translate + ((scale_x_axis == -1) ? m_local_w : 0)),
                m_y + (y_translate + ((scale_y_axis == -1) ? m_local_h : 0)));
    ofScale(scale_x_axis * 1.5, scale_y_axis * 1.5);
    
    m_module->draw();
    
    ofPopMatrix();
    new_fbo.end();
    return new_fbo;
}

ofFbo cineChannel::get_fbo(const float scale_x_axis, const float scale_y_axis) {
    ofFbo new_fbo = m_fbo;
    
    new_fbo.begin();
    ofPushMatrix();
    
    cineUtils::default_background(m_local_w, m_local_h);
    
    m_module->draw();

    ofPopMatrix();
    new_fbo.end();
    
    return new_fbo;
}

ofFbo cineChannel::get_motion_fbo(const float scale_x_axis, const float scale_y_axis) {
    ofFbo new_fbo = m_fbo;
    new_fbo.begin();
    ofPushMatrix();
    cineUtils::default_background(m_local_w, m_local_h);

    ofTranslate((scale_x_axis == -1 ? m_local_w : 0), // removed _axis
                (scale_y_axis == -1 ? m_local_h : 0)); // removed _axis
    ofScale(2 * scale_x_axis, 2 * scale_y_axis); // removed
    
    ofTranslate(m_x, m_y);
    
    m_module->draw();

    ofPopMatrix();
    new_fbo.end();
    
    return new_fbo;
}

ofTexture* cineChannel::getTexture() {
    return m_module->getTexture();
}



int cineChannel::is_active() {
    return m_enabled;
}

void cineChannel::activate(bool value) {
    m_enabled = value;
}

void cineChannel::set_fade_value(const double value) {
    m_fade = value;
}

void cineChannel::set_reversed_fade(const double value) {
    m_reversed_fade = value;
}

double cineChannel::get_fade() {
    return m_fade;
}

double cineChannel::get_reversed_fade() {
    return m_reversed_fade;
}

void cineChannel::draw_reversed(float x, float y) {
    ofPixels inverted_pixels;
    ofTexture texture_tmp;
    
    get_fbo().readToPixels(inverted_pixels);
    inverted_pixels.mirror(true, true);
    texture_tmp.loadData(inverted_pixels);
    texture_tmp.draw(x,y);
}

ofFbo cineChannel::fbo_reversed(float x, float y) {
    ofFbo tmp_fbo, old_fbo;
    ofPixels inverted_pixels;
    ofTexture texture_tmp;
    
    old_fbo = get_fbo();
    tmp_fbo = cineUtils::utils_init_fbo(old_fbo.getWidth(), old_fbo.getHeight());

    tmp_fbo.begin();
    ofPushMatrix();
    ofTranslate(old_fbo.getWidth(), old_fbo.getHeight());
    
    ofScale(-1, -1, 1);
    old_fbo.draw(0,0);
    
    ofPopMatrix();
    tmp_fbo.end();
    
    return tmp_fbo;
}

void cineChannel::add_draw(float x, float y) {
    ofEnableBlendMode(OF_BLENDMODE_SCREEN);
    draw(x,y);
    ofEnableBlendMode(OF_BLENDMODE_DISABLED);
}

vector<ofPolyline> cineChannel::get_lines(float min_th, float max_th){
    vector<ofVec3f> dots;
    vector<ofPolyline> polylines;
    vector<ofVec3f> updated_dots;

    ofVec3f dot;
    ofPolyline line;
    
    ofPixels pixels;
    pixels = m_module->get_pixels();
    
    for (int i = 0; i < pixels.getWidth() - 3; i += 3) {
        for (int j = 0; j < pixels.getHeight() - 3; j += 3) {
            int val = pixels.getColor(i,j)[0];
            if ((val > 255*min_th) && (val < 255*max_th)) {
                dot.set(i,j, 0);
                dots.push_back(dot);
//                line.addVertex(dot);
//                line.lineTo(dot);
//                cout << i << ":" << j << "--" << val << " ; ";
//                cout << dot.x << ":" << dot.y << ":" << dot.z << endl;
            }
        }
    }
        
    cout << "number of dots " << dots.size() << endl;
    
    if (dots.size() > 0)
    {
        for (int a = 0; a < 1000; ++a) {
            updated_dots.push_back(dots[rand() % dots.size()]);
        }
        dots = updated_dots;

        for (int a = 0; a < dots.size()-1; ++a) {
            dot = dots[a];
            float distance_reference = 1000;
            line.clear();
            line.addVertex(dot);
            for (int i = a+i; i < dots.size(); ++i) {
                ofVec3f tmp_dot = dots[i];
                float dist = dot.squareDistance(tmp_dot);
                if (dist < distance_reference) {
                    line.lineTo(tmp_dot);
                }
//                else {
//                    updated_dots.push_back(tmp_dot);
//                }
            }
            line.close();
//            dots = updated_dots;
            polylines.push_back(line);

        }
    }
    return polylines;
};


void cineChannel::increment_local_w() {
    m_local_w += 1;
    cout << "  - w " << m_local_w ;
};
void cineChannel::decrement_local_w(){
    m_local_w -= 1;
    cout << " - w " << m_local_w ;
}

void cineChannel::increment_local_h(){
    m_local_h += 1;
    cout << " - h " << m_local_h ;
}
void cineChannel::decrement_local_h(){
    m_local_h -= 1;
    cout << " - h " << m_local_h ;
}
