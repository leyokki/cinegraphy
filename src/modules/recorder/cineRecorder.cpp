//
//  cineRecorder.cpp
//  midiMapper
//
//  Created by lky on 11/12/2019.
//

#include "cineRecorder.hpp"
#include "ofxFastFboReader.h"

ofxFastFboReader reader;

void cineRecorder::init(string dir, int w, int h){
    /********** RECORDING ********/
    m_is_recording = false;
    
    m_Recorder.setup(true, false, glm::vec2(w, h));
    m_Recorder.setOverWrite(true);
    
    // you don't need to set this if FFMPEG is in your system path //
    // otherwise, don't forget to put an update bin of ffmpeg in ofxFFmpeg
    m_Recorder.setFFmpegPathToAddonsPath();
    // m_Recorder.setVideoCodec("hap");
    m_Recorder.addAdditionalOutputArgument("-c:v hap -r 25");
}

void cineRecorder::record(){
    if( m_Recorder.isRecording() ) {
        // stop
        m_Recorder.stop();
    } else {
#if defined(TARGET_OSX)
        m_Recorder.setOutputPath( ofToDataPath(ofGetTimestampString() + ".mov", true ));
#else
        m_Recorder.setOutputPath( ofToDataPath(ofGetTimestampString() + ".avi", true ));
#endif
        m_Recorder.startCustomRecord();
    }
}

void cineRecorder::update(ofFbo reference){
    if(m_Recorder.isRecording() ) {
        
        reader.readToPixels(reference, mPix);
        
        if( mPix.getWidth() > 0 && mPix.getHeight() > 0 ) {
            m_Recorder.addFrame( mPix );
        }
    }
    
}

void cineRecorder::exit(){
    if( m_Recorder.isRecording() ) {
        // stop
        m_Recorder.stop();
    }
}
