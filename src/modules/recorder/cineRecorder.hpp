//
//  cineRecorder.hpp
//  midiMapper
//
//  Created by lky on 11/12/2019.
//

#ifndef cineRecorder_hpp
#define cineRecorder_hpp

#include "ofMain.h"
#include <stdio.h>
#include "ofxFFmpegRecorder.h"

class cineRecorder: public ofBaseApp {
public:
    
    void init(string dir, int w, int h);
    void update(ofFbo reference);
    void exit();
    
    void record();

private:
    
    ofxFFmpegRecorder m_Recorder;
    ofPixels mPix;
    bool m_is_recording;
    
};

#endif /* cineRecorder_hpp */
