//
//  channel.cpp
//  midiMapper
//
//  Created by lky on 09/12/2019.
//

#include "cineVideo.hpp"
#include "utils/utilities.hpp"

cineVideo::cineVideo(const string path) {
    m_video_list = cineUtils::list_paths(path);
    type_name = "video";
    
    m_video_number = 0;
    tmp_video_number = 0;
    m_speed = 1.0;
    m_opacity = 1.0;
    m_centered = false;
    m_still = false;
    
    for (int i = 0; i < m_video_list.size(); ++i) {
        ofPtr<ofxHapPlayer> v (new ofxHapPlayer());
        
        v->load(m_video_list[i]);
        v->setLoopState(OF_LOOP_NORMAL);
        v->setVolume(.1);
        v->setSpeed(m_speed);
        
        while (!v->isLoaded()) {
            ofSleepMillis(10);
            ofLog() << "sleeping" << std::endl;
        }
        v->play();
        v->stop();
        m_videos.push_back(v);
    };
    
    m_videos[m_video_number]->play();
    
    ofLog() << "cineVideo setup:: " << m_videos[m_video_number]->getMoviePath() << "\n";
} 

cineVideo::~cineVideo() {
    type_name = "undefined";
    ofLog() << "object is deleted" << '\n';
}

void cineVideo::reset(){
    m_videos[m_video_number]->setPosition(0);
    m_videos[m_video_number]->play();
}

void cineVideo::update() {
    if(m_video_number != tmp_video_number) {
        
        ofLog() << "Updating m_video_number to " << tmp_video_number << std::endl;
        
        m_videos[m_video_number]->stop();
        m_video_number = tmp_video_number;
        m_videos[m_video_number]->setSpeed(m_speed);
        m_videos[m_video_number]->play();
    }
    if (m_videos[m_video_number]->isLoaded()) {
           m_videos[m_video_number]->update();
   }
}

void cineVideo::update_all_videos_parameters() {
    for (int i = 0; i < m_video_number; ++i) {
        m_videos[i]->setSpeed(m_speed);
    };
}

void cineVideo::update(const double& m_speed_value, const int& video_number_value) {
    if (m_videos[m_video_number]->isLoaded()) {
        update_speed((float)m_speed_value);
        update_image_selected(video_number_value);
        m_videos[m_video_number]->update();
        if (!m_videos[m_video_number]->isPlaying()) {
            m_videos[m_video_number]->play();
        }
    }
}

void cineVideo::update_still() {
    if(m_video_number != tmp_video_number) {
        ofLog() << "Updating m_video_number to " << tmp_video_number << std::endl;
        m_videos[m_video_number]->stop();
        m_video_number = tmp_video_number;
        m_videos[m_video_number]->setSpeed(m_speed);

    }
    if (m_videos[m_video_number]->isLoaded()) {
        freeze();
        m_videos[m_video_number]->update();
    }
}

void cineVideo::update_still(const double& frame_value, const int& video_number_value) {
    if (m_videos[m_video_number]->isLoaded()) {
        update_frame((float)frame_value);
        update_image_selected(video_number_value);
        m_videos[m_video_number]->update();
    }
}

void cineVideo::draw(const double scale) {
    m_videos[m_video_number]->draw(0, m_centered ? (ofGetHeight() - m_videos[m_video_number]->getHeight())/(2 / scale) : 0);
}

ofTexture * cineVideo::getTexture() {
    return m_videos[m_video_number]->getTexture();
}

void cineVideo::draw(const float x, const float y) {
    m_videos[m_video_number]->draw(x, y);
}

void cineVideo::set_speed(float value) {
    m_speed = value;
}

void cineVideo::freeze(){
    if (m_videos[m_video_number]->isPlaying()) {
        m_videos[m_video_number]->stop();
    }
}

void cineVideo::update_speed(const double& value) {
    if (m_speed != value) {
        ofLogVerbose("Change m_speed \n");
        m_speed = value;
        m_videos[m_video_number]->setSpeed(m_speed);
    }
}

void cineVideo::update_frame(const double& value) {
    double new_val = reverse_value_every_other(value, m_video_number); // to be replaced soon
    new_val = (new_val > 0) ? (new_val < 1) ? new_val : 0.99 : 0.0;
    if (m_videos[m_video_number]->getPosition() != new_val) {
        m_videos[m_video_number]->setPosition(new_val);
        ofLogVerbose("Change frame value to " + to_string(new_val) + "\n");
    }
}

int cineVideo::get_image_selected() {
    return m_video_number;
}

double cineVideo::get_speed_or_frame() {
    return m_still ? m_videos[m_video_number]->getPosition() : m_speed;
}

void cineVideo::update_speed_or_frame(const double& value) {
    m_still ? update_frame(value) : update_speed(value);
}

void cineVideo::update_image_selected(const int& value) {
    cout << "update to val:: " << value << endl;
    int max_video_element = (int)m_video_list.size() - 1;
    tmp_video_number = value < max_video_element ? (value >= 0) ? value : 0 : max_video_element;
}

ofPixels cineVideo::get_pixels() {
    return m_videos[m_video_number]->getPixels();
}

void cineVideo::increment_image() {
    update_image_selected(++tmp_video_number);
}

void cineVideo::decrement_image() {
    update_image_selected(--tmp_video_number);
}
