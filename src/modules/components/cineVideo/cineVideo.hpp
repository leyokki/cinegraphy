//
//  cineVideo.hpp
//  midiMapper
//
//  Created by lky on 09/12/2019.
//

#ifndef cineVideo_hpp
#define cineVideo_hpp

#include "ofMain.h"
#include "modules/cineModule.hpp"
#include "ofxHapPlayer.h"

#include <stdio.h>

class cineVideo: public cineModule {
    
public:
    
    cineVideo(string path);
    virtual ~cineVideo();

    virtual void reset();
    virtual void update();
    virtual void update(const double&, const int&);
    
    virtual void update_still();
    virtual void update_still(const double&, const int&);
    
    virtual void draw(double scale=1);
    virtual void draw(float x, float y);
    
    virtual void increment_image();
    virtual void decrement_image();

    virtual int get_image_selected();
    virtual double get_speed_or_frame();
    
    virtual void update_speed(const double&);
    virtual void update_frame(const double&);

    virtual void update_image_selected(const int& value);
    virtual void update_speed_or_frame(const double& value);

    virtual ofPixels get_pixels();
    virtual ofTexture * getTexture();

    float get_speed() const;
    float get_opacity() const;
    
    void set_speed(float);
    void set_opacity(float);

    void freeze();
    void update_all_videos_parameters();

    
private:
//    ofxHapPlayer m_video;
    vector<ofPtr<ofxHapPlayer>> m_videos;

    bool m_centered;
    bool m_still;
    
    int m_video_number;
    int tmp_video_number;
    
    int m_actual_image;
    
    double m_speed;
    double m_opacity;
    
    vector<string> m_video_list;
};


#endif /* channel_hpp */
