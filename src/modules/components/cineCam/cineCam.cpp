//
//  cineCam.cpp
//  midiMapper
//
//  Created by lky on 15/10/2019.
//

#include "cineCam.hpp"

cineCam::cineCam(int camWidth, int camHeight, int ID) {
    type_name = "cam";
    ofSetVerticalSync(true);
    m_framerate = 30;
    m_device_ID = ID;
    
    vector<ofVideoDevice> devices = vidGrabber.listDevices();
    
    for(size_t i = 0; i < devices.size(); i++){
        if(devices[i].bAvailable){
            //log the device
            ofLogNotice() << devices[i].id << ": " << devices[i].deviceName;
        }else{
            //log the device and note it as unavailable
            ofLogNotice() << devices[i].id << ": " << devices[i].deviceName << " - unavailable ";
        }
    }
    vidGrabber.setDeviceID(m_device_ID);
    vidGrabber.setDesiredFrameRate(m_framerate);
    vidGrabber.initGrabber(camWidth, camHeight);
    
//    videoInverted.allocate(camWidth, camHeight, OF_PIXELS_RGB);
//    videoTexture.allocate(videoInverted);
//    ofSetVerticalSync(true);
    
    cameraFrame = 0;
    desiredFPS = 0;

    cout << "cineCam initialized" << '\n';

    
    cout << "Creating cineCam'\n'";
}

cineCam::~cineCam() {
    cout << "Detroying cineCam" << '\n';
}

//void cineCam::init(int camWidth, int camHeight, int ID) {
//}

void cineCam::reset(){
    // Do nothing
}

void cineCam::update(){
    if (cameraFrame >= desiredFPS) {
        vidGrabber.update();
        cameraFrame = 0;
    }
    cameraFrame++;
    
//    videoInverted = vidGrabber.getPixels();
//    videoInverted.mirror(true, true);
//    videoTexture.loadData(videoInverted);
}

void cineCam::update(const double& x,const int& y){
    update();
}

void cineCam::update_still() {
    update();
}

void cineCam::update_still(const double& x,const int& y) {
    update();
}

void cineCam::draw(double scale) {
    //    vidGrabber.getTexture().drawSubsection(100, 120,1280,426,0, 0);
    vidGrabber.draw(0, 0);
}

void cineCam::draw(float x, float y) {
    //    vidGrabber.getTexture().drawSubsection(100, 120,1280,426,0, 0);
    vidGrabber.draw(x, y);
}

ofPixels cineCam::get_pixels() {
    return vidGrabber.getPixels();
}

ofTexture * cineCam::getTexture() {
    ofTexture* someTexture;
    return someTexture;
}


void cineCam::update_speed_or_frame(const double& value) {
    m_framerate = value;
    vidGrabber.setDesiredFrameRate(m_framerate);
}

void cineCam::update_image_selected(const int& value) {
    vidGrabber.setDeviceID(value);
}

int cineCam::get_image_selected() {
    return m_device_ID;
}

double cineCam::get_speed_or_frame() {
    return m_framerate;
}

void cineCam::increment_image() {
    cerr << "Increment does nothing for cineCam" << endl;
}

void cineCam::decrement_image() {
    cerr << "Decrement does nothing for cineCam" << endl;
}

void cineCam::update_speed(const double& value) {
    m_framerate = value;
    vidGrabber.setDesiredFrameRate(m_framerate);
}

void cineCam::update_frame(const double& frame) {
    cerr << "Update frame does nothing for cineCam" << endl;
}
