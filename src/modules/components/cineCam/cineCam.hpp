//
//  cineCam.hpp
//  midiMapper
//
//  Created by lky on 15/10/2019.
//

#ifndef cineCam_hpp
#define cineCam_hpp

#pragma once

#include <stdio.h>
#include "ofMain.h"
#include "modules/cineModule.hpp"

class cineCam: public cineModule {
    
public:
    
    cineCam(int camWidth = 1280, int camHeight = 720, int ID = 0);
    virtual ~cineCam();
    virtual void reset();
    
    virtual void update();
    virtual void update(const double&, const int&);
    
    virtual void update_still();
    virtual void update_still(const double&, const int&);
    
    virtual void draw(double scale);
    virtual void draw(float x, float y);
    
    virtual void increment_image();
    virtual void decrement_image();
    
    virtual int get_image_selected();
    virtual double get_speed_or_frame();
    
    virtual void update_speed(const double&);
    virtual void update_frame(const double&);
    
    virtual void update_image_selected(const int& value);
    virtual void update_speed_or_frame(const double& value);
        
    virtual ofPixels get_pixels();
    virtual ofTexture* getTexture();
        
private:
    ofVideoGrabber vidGrabber;
    ofPixels videoInverted;
    ofTexture videoTexture;
    vector<ofVideoDevice> m_list_of_devices;
    
    vector<ofVideoGrabber> m_webcams;
    vector<int> is_available;
    
    bool m_still;
    
    int m_active_device_ID;
    int m_framerate;
    int m_device_ID;
    
    int camWidth;
    int camHeight;
    
    int cameraFrame;
    int desiredFPS;
};


#endif /* cineCam_hpp */
