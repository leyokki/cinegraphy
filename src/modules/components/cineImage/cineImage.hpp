//
//  imageComponent.hpp
//  midiMapper
//
//  Created by lky on 09/12/2019.
//

#ifndef cineImage_hpp
#define cineImage_hpp

#include "ofMain.h"
#include "modules/cineModule.hpp"
//#include "State.hpp"

#include <stdio.h>

class cineImage : public cineModule {
public:
    
    cineImage(string path);
    virtual ~cineImage();
    virtual void reset();
    
    void setup(string path); // type, path?
    void set_image_list();
    void update_screenImage();
    
    virtual void update();
    virtual void update(const double&, const int&);

    virtual void update_still();
    virtual void update_still(const double&, const int&);

    virtual void draw(double scale=1);
    virtual void draw(float x, float y);

    virtual int get_image_selected();
    virtual double get_speed_or_frame();

    virtual void update_image_selected(const int& value);
    virtual void update_speed_or_frame(const double& value);

    virtual ofPixels get_pixels();
    virtual ofTexture* getTexture();

    float get_speed() const;
    float get_opacity() const;

    void set_speed(float);
    void set_opacity(float);

    virtual void update_speed(const double&);
    virtual void update_frame(const double&);

    virtual void increment_image();
    virtual void decrement_image();
    
    void record_sequence();
    void play_sequence();
    
    
//    shared_ptr<State> sharedState;
    
private:
    ofImage image;
    
    bool centered;
    
    int image_number;
    
    int actualFrame;
    bool isRecording;
    bool playRecord;
    int nextFrame;
    int keyframe;

    vector<string> image_list;
    
};

#endif /* imageComponent_hpp */
