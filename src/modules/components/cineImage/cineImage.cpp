//
//  imageComponent.cpp
//  midiMapper
//
//  Created by lky on 09/12/2019.
//

#include "cineImage.hpp"

cineImage::cineImage(const string path) {
    image.load(path);
    
//    vector<ofImage> import_images(string dir) {
//        // Note: seem not to be able to read filenames with letter such as 'È'
//        
//        vector<ofImage> list_of_ofImages;
//        ofDirectory img_dir;
//
//        img_dir.allowExt("jpg");
//        img_dir.open(dir);
//        img_dir.listDir();
//
//        vector<string> paths;
//
//        for (int i = 0; i < img_dir.size(); ++i) {
//            string path = img_dir.getPath(i);
//            paths.push_back(path); // <!> cannot check if src is correct, should be done later. <!>
//        }
//        std::sort(paths.begin(),paths.end());
//
//        for (int i = 0; i < img_dir.size(); ++i) {
//            ofLogVerbose(img_dir.getPath(i));
//            ofImage src;
//            bool succ = src.load(paths[i]);
//            if (succ) list_of_ofImages.push_back(src);
//        }
//
//        return list_of_ofImages;
//    }

}


void cineImage::reset(){
    // Do nothing
}

void cineImage::setup(const string path) {
    cout << path << endl;
}

cineImage::~cineImage() {
    type_name = "undefined";
    std::cout<< "object is deleted" << '\n';
}

void cineImage::update() {
}

void cineImage::update(const double& m_speed_value, const int& video_number_value) {
}

void cineImage::update_still() {
}

void cineImage::update_still(const double& frame_value, const int& video_number_value) {
}

void cineImage::draw(const double scale) {
}

void cineImage::draw(const float x, const float y) {
}

void cineImage::set_speed(float value) {
}


void cineImage::update_speed(const double& value) {
}

void cineImage::update_frame(const double& value) {
}

int cineImage::get_image_selected() {
}

double cineImage::get_speed_or_frame() {
}

void cineImage::update_speed_or_frame(const double& value) {
}

void cineImage::update_image_selected(const int& value) {
}

ofPixels cineImage::get_pixels() {
}

void cineImage::increment_image() {
}

void cineImage::decrement_image() {
}

ofTexture * cineImage::getTexture() {
    ofTexture* someTexture;
    return someTexture;
}
