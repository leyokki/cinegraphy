//
//  cineModule.cpp
//  midiMapper
//
//  Created by lky on 21/12/2019.
//

#include "cineModule.hpp"
#include "utils/utilities.hpp"

cineModule::cineModule() {
    type_name = "undefined";
    ofLog() << "object is created" << '\n' << "but is empty" << '\n';
}

cineModule::~cineModule(){
    ofLog() << "module destroyed \n";
}

string cineModule::get_type() {
    return type_name;
}

