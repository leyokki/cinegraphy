//
//  cineMidi.hpp
//  midiMapper
//
//  Created by lky on 15/10/2019.
//

#ifndef cineMidi_hpp
#define cineMidi_hpp

#pragma once

#include "ofMain.h"
#include "ofxMidi.h"

#include "cineConsole.hpp"
#include <stdio.h>

class cineMidi: public ofBaseApp, public ofxMidiListener {
    
public:
    void newMidiMessage(ofxMidiMessage& eventArgs);
    
    void setup(cineConsole* console_reference);
    void setup(cineConsole* console_reference, const string& keymap_path);
    
    void update();
    void draw();
    void midiAction(int action, int value);
    void midiOffAction(int action, int value);
    void midiControleAction(int action, int value);
    void exit();
    
    void default_action();
    
    ofxMidiIn midiIn;
    std::vector<ofxMidiMessage> midiMessages;
    
    void load_xml_keymap(string path);
    void write_xml_keymap(string name);
    
    void automate_remap(int amplitude = 10, int min_value = 0);
    void init_keymap();
    void apply_keymapped_key_function(int);
    void apply_keymapped_offkey_function(int);
    void apply_keymapped_control_function(const int&, const int&);

private:
    
    cineConsole* m_console_reference;
    
    static const int keymap_number_of_keys = 88;
    static const int keymap_number_of_variables = 3;
    std::array<std::array<string, keymap_number_of_variables>, keymap_number_of_keys> midi_keymap;
    
    static const int midi_control_key_number = 8;
    string midi_control_keymap[midi_control_key_number][4];

    
    int switch_val;
};

#endif /* cineMidi_hpp */
