//
//  cineMidi.cpp
//  midiMapper
//
//  Created by lky on 15/10/2019.
//

#include "cineMidi.hpp"
#include "utils/cine_functions_enum.hpp"

void cineMidi::setup(cineConsole *consoleReference){
    midiIn.listInPorts();
    midiIn.openPort(0);
    midiIn.ignoreTypes(false, false, false);
    midiIn.addListener(this);
    midiIn.setVerbose(true);
    
    m_console_reference = consoleReference;
  
    init_keymap();
    automate_remap(11,48);
    load_xml_keymap("midi_keymap");
    
    switch_val = 0;
    write_xml_keymap("random");
}

void cineMidi::setup(cineConsole *consoleReference, const string& keymap_path){
    midiIn.listInPorts();
    midiIn.openPort(0);
    midiIn.ignoreTypes(false, false, false);
    midiIn.addListener(this);
    midiIn.setVerbose(true);
    
    m_console_reference = consoleReference;
  
    init_keymap();
    automate_remap(11,48);
    load_xml_keymap(keymap_path);
}

void cineMidi::exit() {
    midiIn.closePort();
    midiIn.removeListener(this);
}

//--------------------------------------------------------------

void cineMidi::newMidiMessage(ofxMidiMessage& msg) {
    // mapping the midiActions array to the actual midi
    int set_pitch = 48;
    
    if (msg.status == MIDI_NOTE_ON) {
        if (msg.pitch-set_pitch >= 0) {
            apply_keymapped_key_function(msg.pitch);
        }
    } else if (msg.status == MIDI_NOTE_OFF) {
        if (msg.pitch-set_pitch > 0) {
            apply_keymapped_offkey_function(msg.pitch);
//            m_console_reference->update_channel_image(<#const int channel#>, <#const int value#>)
        }
    } else if(msg.status == MIDI_CONTROL_CHANGE) {
        apply_keymapped_control_function(msg.control,msg.value);
//        midiControleAction(msg.control,msg.value); // control K1, K2... ; value 0 - 127
    }
}

void cineMidi::update() {
}

void cineMidi::draw() {
}

void cineMidi::default_action() {
    // void();
    // add blank note, when key is released.
}

void cineMidi::midiOffAction(int action, int value) {
    switch (action) {
        case 12:
            m_console_reference->activate_channel(3, false);
            m_console_reference->increment_channel_image(3);
            break;
        default:
            break;
    }
}

void cineMidi::midiControleAction(int action, int value){
}

//-------------------------------------------------------------- KEYMAP

void cineMidi::init_keymap(){
    for (int i = 0; i < keymap_number_of_keys; ++i) {
        midi_keymap[i][0] = "none";
        midi_keymap[i][1] = "undefined_value";
        midi_keymap[i][2] = "undefined_value";
    }
}

void cineMidi::automate_remap(int amplitude, int min_value) {
    for (int i = 0; i < amplitude; ++i) {
        midi_keymap[min_value + i][0] = "update_channel_image";
        midi_keymap[min_value + i][1] = "0";
        midi_keymap[min_value + i][2] = to_string(i);
    }
}

void cineMidi::apply_keymapped_key_function(int value) {
    try {
        switch(check_cine_function(midi_keymap[value][0])) {
            case none:
                ofLog() << value << " :: " << midi_keymap[value][0] << " - Nothing to do" << std::endl;
                ofLog() << "values :: " << midi_keymap[value][1] << " and " << midi_keymap[value][2] << endl;
                break;
            case update_channel_image:
                std::cout << "update_channel_image" << std::endl;
                m_console_reference->update_channel_image(std::stoi(midi_keymap[value][1]),std::stoi(midi_keymap[value][2]));
                break;
            case activate_channel:
                std::cout << "activate_channel" << std::endl;
                m_console_reference->activate_channel(std::stoi(midi_keymap[value][1]),1);
                break;
            default:
                break;
        }
    }
    catch (...) {
        std::cerr << "MIDI SHORTCUTS:: Non recognized function or non valid variables" << std::endl;
    }
}

void cineMidi::apply_keymapped_offkey_function(int value) {
    try {
        switch(check_cine_function(midi_keymap[value][0])) {
            case none:
                ofLog() << value << " :: " << midi_keymap[value][0] << " - Nothing to do" << std::endl;
                ofLog() << "values :: " << midi_keymap[value][1] << " and " << midi_keymap[value][2] << endl;
                break;
            case update_channel_image:
                ofLog() << "update_channel_image" << std::endl;
                m_console_reference->update_channel_image(std::stoi(midi_keymap[value][1]),0);
                break;
            case activate_channel:
                cout << "deactivate_channel" << std::endl;
                m_console_reference->activate_channel(std::stoi(midi_keymap[value][1]),0);
                break;
            default:
                break;
        }
    }
    catch (...) {
        ofLog() << "MIDI SHORTCUTS:: Non recognized function or non valid variables" << std::endl;
    }
}

void cineMidi::apply_keymapped_control_function(const int& control_number, const int& value) {
    try {
        switch(check_cine_function(midi_control_keymap[control_number][0])) {
            case none:
                ofLog() << control_number << " :: " << midi_keymap[control_number][0] << " - Nothing to do" << std::endl;
                ofLog() << "values :: " << midi_keymap[control_number][1] << " and " << midi_keymap[control_number][2] << std::endl;
                break;
            case set_saturation:
                ofLog() << "set_saturation" << std::endl;
                m_console_reference->set_saturation(ofMap(value, 0, 127, std::stof(midi_control_keymap[control_number][2]),std::stof(midi_control_keymap[control_number][3])));
                break;
            case set_contrast:
                ofLog() << "set_saturation" << std::endl;
                m_console_reference->set_contrast(ofMap(value, 0, 127, std::stof(midi_control_keymap[control_number][2]),std::stof(midi_control_keymap[control_number][3])));
                break;
            case set_brightness:
                ofLog() << "set_brightness" << std::endl;
                m_console_reference->set_brightness(ofMap(value, 0, 127, std::stof(midi_control_keymap[control_number][2]),std::stof(midi_control_keymap[control_number][3])));
                break;
            case update_channel_speed_or_frame:
                ofLog() << "update_channel_speed_or_frame" << std::endl;
                m_console_reference->update_channel_speed_or_frame(std::stoi(midi_control_keymap[control_number][1]), ofMap(value, 0, 127, std::stof(midi_control_keymap[control_number][2]),std::stof(midi_control_keymap[control_number][3])));
                break;
            case update_channel_fade:
                ofLog() << "update_channel_fade" << std::endl;
                m_console_reference->update_channel_fade(std::stoi(midi_control_keymap[control_number][1]), ofMap(value, 0, 127, std::stof(midi_control_keymap[control_number][2]),std::stof(midi_control_keymap[control_number][3])));
                break;
            default:
                break;
        }
    }
    catch (...) {
        cout << "MIDI SHORTCUTS:: Non recognized function or non valid variables" << endl;
    }
}

void cineMidi::load_xml_keymap(string path) {
    ofxXmlSettings xml_midi_key_presets;
    
    if (xml_midi_key_presets.loadFile("_xml/keymap/"+path+".xml")) {
        ofLog() << "Midi xml loaded! :: " << path << '\n';
    } else {
        ofLog() << "unable to load " + path + " check _xml/ folder" << '\n';
    }

    xml_midi_key_presets.pushTag("MIDI_NOTE");

    int number_of_keys = xml_midi_key_presets.getNumTags("KEY");

    for (int i = 0; i < number_of_keys; ++i) {
        xml_midi_key_presets.pushTag("KEY",i);
        int key = xml_midi_key_presets.getValue("PITCH", 0);
        string xml_function = xml_midi_key_presets.getValue("FUNCTION", "none");
        string xml_channel = xml_midi_key_presets.getValue("CHANNEL", "undefined");
        string xml_value = xml_midi_key_presets.getValue("VALUE", "undefined");
        xml_midi_key_presets.popTag();
        
        if (key != 0) {
            midi_keymap[key][0] = xml_function;
            midi_keymap[key][1] = xml_channel;
            midi_keymap[key][2] = xml_value;
        }
    }
    
    xml_midi_key_presets.popTag();
    xml_midi_key_presets.pushTag("MIDI_CONTROL");

    int defined_control = xml_midi_key_presets.getNumTags("CONTROL_ACTION");
    
    for (int i = 0; i < defined_control; ++i) {
        xml_midi_key_presets.pushTag("CONTROL_ACTION",i);
        int control = xml_midi_key_presets.getValue("CONTROL_NUMBER", 0);
        string xml_function = xml_midi_key_presets.getValue("FUNCTION", "none");
        string xml_channel = xml_midi_key_presets.getValue("CHANNEL", "undefined");
        string xml_value_1 = xml_midi_key_presets.getValue("VALUE_1", "undefined");
        string xml_value_2 = xml_midi_key_presets.getValue("VALUE_2", "undefined");
        xml_midi_key_presets.popTag();

        if (control < midi_control_key_number) {
            midi_control_keymap[control][0] = xml_function;
            midi_control_keymap[control][1] = xml_channel;
            midi_control_keymap[control][2] = xml_value_1;
            midi_control_keymap[control][3] = xml_value_2;
            
            ofLog() << "Writing:: " << control <<  " " << xml_function << " - " << xml_channel << " _ " << xml_value_1 << " to " << xml_value_2 << std::endl;
        }
    }
    xml_midi_key_presets.popTag();

}

void cineMidi::write_xml_keymap(string name) {
    ofxXmlSettings xml_midi_key_presets;

    for (int i = 0; i < keymap_number_of_keys; ++i) {
        
        xml_midi_key_presets.addTag("KEY");
        xml_midi_key_presets.pushTag("KEY",i);
        
        xml_midi_key_presets.addValue("PITCH", i);
        xml_midi_key_presets.addValue("FUNCTION", midi_keymap[i][0]);
        xml_midi_key_presets.addValue("CHANNEL", midi_keymap[i][1   ]);
        xml_midi_key_presets.addValue("VALUE", midi_keymap[i][2]);
        
        xml_midi_key_presets.popTag();
    }
    
    xml_midi_key_presets.saveFile("_xml/keymap/" + name + ".xml");
}
