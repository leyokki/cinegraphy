//
//  cineDuino.hpp
//  midiMapper
//
//  Created by lky on 16/10/2019.
//

#ifndef cineDuino_hpp
#define cineDuino_hpp

#pragma once

#include "ofMain.h"
//#include "State.hpp"

#include "ofxHapPlayer.h"

#include <stdio.h>

class cineDuino {
    void init_arduino();
    void update_arduino();
    
    // ARDUINO
    ofArduino    ard;
    bool        bSetupArduino;            // flag variable for setting up arduino once
    
    struct buttonStruct {
        int pinNumber;
        bool isPressed;
        unsigned long rebounceTime;
        int clickCount;
    };
    
    void stateOfButton(buttonStruct *someButton);
    float potar_0, potar_1, potar_2, potar_4, potar_10, potar_11, potar_12, potar_14;
    int buttonNumber;
    float factor_speed_ch1, speed_ch1, speed_ch2;
    ofxHapPlayer hap_ch1,hap_ch2,hap_ch3,hap_ch4;
    buttonStruct * list_of_buttons;
    
    void setupArduino(const int & version);
    void digitalPinChanged(const int & pinNum);
    void analogPinChanged(const int & pinNum);
    void updateArduino();
    
    string buttonState;
    string potValue;
};

#endif /* cineDuino_hpp */
