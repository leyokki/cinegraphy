//
//  cineGui.cpp
//  midiMapper
//
//  Created by lky on 13/10/2019.
//

#include "cineGui.hpp"

void cineGui::setup(){
    
    parameters.setName("parameters");
//    parameters.add(sharedState->blah.set("blah",blob,1,100));
    parameters.add(radius.set("radius",50,1,100));
    parameters.add(color.set("color",100,ofColor(0,0),255));
    gui.setup(parameters);
    ofBackground(0);
    ofSetVerticalSync(false);
    
//    obj.set(300, 50, 100, 200);
    
    inner_window_size = ofGetWindowHeight()*2/3;
    
//    test = sharedState->get_actualImage();
//    actualPos = sharedState->get_actualPos();

    gui_scale = 0.01;
    
//    tie(X_pos, Y_pos) = sharedState->get_allPositions();
    
    obj.setup();
    obj.setPosition(50, 50);
    obj.setSize(50, 300);
    
    ofSetColor(255, 255, 255);
//    gui_fbo = utils_init_fbo(1440,1080);
    update_fbo();
}

void cineGui::update(){
//    test = sharedState->get_actualImage();
//    tmpPos = sharedState->get_actualPos();
    if (actualPos != tmpPos) {
        actualPos = tmpPos;
        update_fbo();
    }
    // note:: should create thumbnail.
}

void cineGui::draw(){
    ofSetColor(255);
    
    gui_fbo.draw(0,0);
    
    gui.draw();
    ofSetWindowTitle(ofToString(blob) + "value and " + ofToString(blob_param));
    
//    obj.draw();
    
    stringstream s;
    s << "isMouseOver: " << obj.isMouseOver() << endl;
    s << "isMousePressed(0): " << obj.isMousePressed(0) << endl;
    s << "isMousePressed(1): " << obj.isMousePressed(1) << endl;
    s << "isMousePressed(2): " << obj.isMousePressed(2) << endl;
    s << "getStateChangeMillis(): " << obj.getStateChangeMillis() << endl;

    ofSetColor(255);
    ofDrawBitmapString(s.str(), 20, 30);

}

void cineGui::windowResized(int w, int h) {
    inner_window_size = ofGetWindowHeight()*2/3;
}

void cineGui::update_fbo() {
//    int max_width = 200;
//    int result_height = max_width * test.getHeight() / test.getWidth();
//
//    gui_fbo.begin();
//    test.draw(0,0, max_width, result_height);
//    ofSetColor(255, 0, 0);
//    ofNoFill();
//    for (int i = 0; i < X_pos[0].size(); ++i) {
//        ofDrawRectangle(ofMap(-X_pos[0][i],0, test.getWidth(), 0, max_width), ofMap(-Y_pos[0][i], 0, test.getHeight(), 0, result_height), 200, 200);
//    }
//    ofSetColor(0, 0, 255);
//    ofDrawRectangle(ofMap(-X_pos[0][actualPos],0, test.getWidth(), 0, max_width), ofMap(-Y_pos[0][actualPos], 0, test.getHeight(), 0, result_height), 200, 200);
//    gui_fbo.end();
}

void cineGui::keyPressed(int key) {
    if (key =='s') {
        obj.enabled = 0;
    }
    if (key =='d') {
        obj.enabled = 1;
    }
}
