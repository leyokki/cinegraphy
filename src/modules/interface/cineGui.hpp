//
//  cineGui.hpp
//  midiMapper
//
//  Created by lky on 13/10/2019.
//
#pragma once

#include "ofMain.h"
#include "ofxGui.h"
//#include "State.hpp"

#include "modules/interface/DraggableRect.hpp"

class cineGui: public ofBaseApp {
public:
    void setup();
    void update();
    void draw();
    void windowResized(int w, int h);
    void update_fbo();
    void keyPressed(int);
    
    ofParameterGroup parameters;
    ofParameter<float> radius;
    ofParameter<float> blob_param;
    ofParameter<ofColor> color;
    ofxPanel gui;
    
    int blob;
    ofImage test;
    ofFbo gui_fbo;
    int inner_window_size;

    int gui_scale;
    int actualPos, tmpPos;
//    shared_ptr<State> sharedState;
    vector<vector<int>> X_pos, Y_pos;
    
    DraggableRect obj;

};
