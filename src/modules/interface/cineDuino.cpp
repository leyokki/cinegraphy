//
//  arduino.cpp
//  midiMapper
//
//  Created by lky on 30/09/2019.
//
#include "cineDuino.hpp"

#define ARDUINO
#define REBOUNCE_DELAY_MS 10

void cineDuino::init_arduino() {
    ard.connect("COM5", 57600);
    ard.sendFirmwareVersionRequest();
    ofAddListener(ard.EInitialized, this, &cineDuino::setupArduino);
    bSetupArduino = false;
    
    factor_speed_ch1 = 1.0; // no idea!
    speed_ch1 = 1.0;
}

//--------------------------------------------------------------
void cineDuino::setupArduino(const int & version) {
    
    // remove listener because we don't need it anymore
    ofRemoveListener(ard.EInitialized, this, &cineDuino::setupArduino);
    
    // it is now safe to send commands to the Arduino
    bSetupArduino = true;
    
    // print firmware name and version to the console
    ofLogNotice() << ard.getFirmwareName();
    ofLogNotice() << "firmata v" << ard.getMajorFirmwareVersion() << "." << ard.getMinorFirmwareVersion();
    
    // Note: pins A0 - A5 can be used as digital input and output.
    // Refer to them as pins 14 - 19 if using StandardFirmata from Arduino 1.0.
    // If using Arduino 0022 or older, then use 16 - 21.
    // Firmata pin numbering changed in version 2.3 (which is included in Arduino 1.0)
    
    buttonNumber = 10;
    list_of_buttons = new buttonStruct[buttonNumber];
    for (int i = 0; i < buttonNumber; i++)
    {
        list_of_buttons[i].isPressed = false;
        list_of_buttons[i].rebounceTime = 0;
        list_of_buttons[i].clickCount = 0;
    }
    
    //// Initialiser les numeros specifiques des entrees utilises
    list_of_buttons[0].pinNumber = 2;
    list_of_buttons[1].pinNumber = 3;
    list_of_buttons[2].pinNumber = 4;
    list_of_buttons[3].pinNumber = 5;
    list_of_buttons[4].pinNumber = 6;
    list_of_buttons[5].pinNumber = 7;
    list_of_buttons[6].pinNumber = 10;
    list_of_buttons[7].pinNumber = 11;
    list_of_buttons[8].pinNumber = 12;
    list_of_buttons[9].pinNumber = 13;
    
    //// Initialiser les PIN de tous les boutons  grer
    for (int i = 0; i < buttonNumber; i++)
    {
        ard.sendDigitalPinMode(list_of_buttons[i].pinNumber, ARD_INPUT);
        // pinMode(list_of_buttons[i].entreeDigitale, INPUT);
    }
    
    // set pins D2 and A5 to digital input
    //ard.sendDigitalPinMode(2, ARD_INPUT);
    //ard.sendDigitalPinMode(3, ARD_INPUT);
    //ard.sendDigitalPinMode(4, ARD_INPUT);
    //ard.sendDigitalPinMode(5, ARD_INPUT);
    //ard.sendDigitalPinMode(6, ARD_INPUT);
    //ard.sendDigitalPinMode(19, ARD_INPUT);  // pin 21 if using StandardFirmata from Arduino 0022 or older
    
    // set pin A0 to analog input
    ard.sendAnalogPinReporting(0, ARD_ANALOG);
    ard.sendAnalogPinReporting(1, ARD_ANALOG);
    ard.sendAnalogPinReporting(2, ARD_ANALOG);
    ard.sendAnalogPinReporting(3, ARD_ANALOG);
    ard.sendAnalogPinReporting(4, ARD_ANALOG);
    ard.sendAnalogPinReporting(5, ARD_ANALOG);
    ard.sendAnalogPinReporting(6, ARD_ANALOG);
    ard.sendAnalogPinReporting(7, ARD_ANALOG);
    
    // set pin D13 as digital output
    // ard.sendDigitalPinMode(13, ARD_OUTPUT);
    // set pin A4 as digital output
    ard.sendDigitalPinMode(18, ARD_OUTPUT);  // pin 20 if using StandardFirmata from Arduino 0022 or older
    
    // set pin D11 as PWM (analog output)
    // ard.sendDigitalPinMode(11, ARD_PWM);
    
    // attach a servo to pin D9
    // servo motors can only be attached to pin D3, D5, D6, D9, D10, or D11
    // ard.sendServoAttach(9);
    
    // Listen for changes on the digital and analog pins
    //ofAddListener(ard.EDigitalPinChanged, this, &cineDuino::digitalPinChanged);
    ofAddListener(ard.EAnalogPinChanged, this, &cineDuino::analogPinChanged);
}

//--------------------------------------------------------------
void cineDuino::updateArduino(){
    // update the arduino, get any data or messages.
    // the call to ard.update() is required
    ard.update();
    
    // do not send anything until the arduino has been set up
    if (bSetupArduino) {
        // fade the led connected to pin D11
        for (int i = 0; i < buttonNumber; ++i) {
            stateOfButton(&(list_of_buttons[i]));
        }
        
        /*if (ard.getDigital(4)) {
         list_of_buttons[3].clickCount++;
         }*/
        
        //        // REVERSE SPEED
        //        if (list_of_buttons[0].isPressed) {
        //            factor_speed_ch1 = -factor_speed_ch1;
        //            speed_ch1 = factor_speed_ch1*speed_ch1;
        //            hap_ch1.setSpeed(speed_ch1);
        //        }
        //        if (list_of_buttons[1].isPressed) {
        //            speed_ch2 = factor_speed_ch2*speed_ch2;
        //            hap_ch2.setSpeed(speed_ch2);
        //        }
        //
        //        // START - STOP button
        //        if (list_of_buttons[2].isPressed) {
        //            if (hap_ch1.isPlaying()) {
        //                hap_ch1.stop();
        //            }
        //            else {
        //                hap_ch1.play();
        //            }
        //        }
        //        if (list_of_buttons[3].isPressed) {
        //            if (hap_ch2.isPlaying()) {
        //                hap_ch2.stop();
        //            }
        //            else {
        //                hap_ch2.play();
        //            }
        //        }
        //        if (list_of_buttons[4].isPressed) {
        //            if (hap_ch3.isPlaying()) {
        //                hap_ch3.stop();
        //            }
        //            else {
        //                hap_ch3.play();
        //            }
        //            if (hap_ch1.isPlaying()) {
        //                hap_ch1.stop();
        //            }
        //            else {
        //                hap_ch1.play();
        //            }
        //            if (hap_ch2.isPlaying()) {
        //                hap_ch2.stop();
        //            }
        //            else {
        //                hap_ch2.play();
        //            }
        //        }
        //
        //        // START AGAIN CHANNEL
        //        if (list_of_buttons[5].isPressed) {
        //            hap_ch1.setFrame(0);
        //        }
        //        if (list_of_buttons[6].isPressed) {
        //            hap_ch2.setFrame(0);
        //        }
        //        if (list_of_buttons[7].isPressed) {
        //            hap_ch1.setFrame(0);
        //            hap_ch2.setFrame(0);
        //            hap_ch3.setFrame(0);
        //        }
        
    }
    
}

//--------------------------------------------------------------
void cineDuino::digitalPinChanged(const int & pinNum) {
    // do something with the digital input. here we're simply going to print the pin number and
    // value to the screen each time it changes
    //buttonState = "digital pin: " + ofToString(pinNum) + " = " + ofToString(ard.getDigital(pinNum));
}

void cineDuino::stateOfButton(buttonStruct *someButton) {
    bool signalState = !ard.getDigital(someButton->pinNumber);
    if (signalState) {
        if (someButton->rebounceTime != 0) {
            unsigned long elapsedTime = ofGetElapsedTimeMillis() - someButton->rebounceTime;
            
            if (elapsedTime >= REBOUNCE_DELAY_MS) {
                someButton->isPressed = true;
                someButton->rebounceTime = 0;
                someButton->clickCount++;
            }
        }
        else if (!someButton->isPressed) {
            someButton->rebounceTime = ofGetElapsedTimeMillis();
        }
    }
    else {
        if (someButton->isPressed) {
            someButton->isPressed = false;
        }
        else {
            someButton->rebounceTime = 0;
        }
    }
}


float smoothing_speed(float x, int max) {
    return max * sin(x*x) * sin (x*x);
}

//--------------------------------------------------------------
void cineDuino::analogPinChanged(const int & pinNum) {
    
    potValue = "analog pin: " + ofToString(pinNum) + " = " + ofToString(ard.getAnalog(pinNum));
    
    /* to check */
    if (pinNum == 44) {
        potar_0 = ofMap(ard.getAnalog(pinNum), 0,1024,-PI/2,PI/2);
        //if ((int)potar_0*10 != (int)speed_ch1 * 10) {
        potar_0 = roundf(potar_0 * 10)/10;
        // CHECK speed != 0....
        if ((potar_0 < 0.1) && (potar_0 > -0.1)) {
            hap_ch1.stop();
        }
        if (potar_0 != factor_speed_ch1 * speed_ch1) {
            speed_ch1 = factor_speed_ch1 * smoothing_speed(potar_0,3);
            speed_ch1 = factor_speed_ch1*tan(potar_0); // use tan function to smooth the curve
            hap_ch1.setSpeed(speed_ch1);
            if (!hap_ch1.isPlaying()) {
                hap_ch1.stop();
            }
        }
        //}
    }
    
    //std::cout << potValue << endl;
    if (pinNum == 0) {
        potar_0 = ofMap(ard.getAnalog(pinNum), 0,1024,-2,2);
        //if ((int)potar_0*10 != (int)speed_ch1 * 10) {
        potar_0 = roundf(potar_0 * 10)/10;
        // CHECK speed != 0....
        if ((potar_0 < 0.1) && (potar_0 > -0.1)) {
            potar_0 = 0.1;
        }
        if (potar_0 != factor_speed_ch1 * speed_ch1) {
            speed_ch1 = factor_speed_ch1*potar_0;
            hap_ch1.setSpeed(speed_ch1);
        }
        //}
    }
    if (pinNum == 1) {
        potar_1 = ofMap(ard.getAnalog(pinNum), 0, 1024, -2, 2);
        //if ((int)potar_1 * 10 != (int)speed_ch2 * 10) {
        potar_1 = roundf(potar_1 * 10) / 10;
        // CHECK speed != 0....
        if ((potar_1 < 0.1) && (potar_1 > -0.1)) {
            potar_1 = 0.1;
        }
        if (potar_1 != factor_speed_ch1 * speed_ch2) {
            speed_ch2 = factor_speed_ch1 * potar_1;
            hap_ch2.setSpeed(speed_ch2);
        }
        //}
    }
    //
    //    if (pinNum == 2) {
    //        potar_2 = ofMap(ard.getAnalog(pinNum), 0, 1024, 0, 1);
    //        //potar_10 = roundf(potar_10 * 10) / 10;
    //        opBC_ch1 = potar_2;
    //    }
    //    if (pinNum == 3) {
    //        potar_4 = ofMap(ard.getAnalog(pinNum), 0, 1024, 0, 1);
    //        //potar_11 = roundf(potar_11 * 10) / 10;
    //        opBC_ch2 = potar_4;
    //    }
    //    if (pinNum == 4) {
    //        potar_10 = ofMap(ard.getAnalog(pinNum), 0, 1024, -1000, 1000);
    //        //potar_12 = roundf(potar_12 * 10) / 10;
    //        x_pos_ch1 = potar_10;
    //    }
    //    if (pinNum == 5) {
    //        potar_11 = ofMap(ard.getAnalog(pinNum), 0, 1024, -1000, 1000);
    //        x_pos_ch2 = potar_11;
    //        //potar_14 = roundf(potar_14 * 10) / 10;
    //        // extension_3channel to add
    //    }
    //    if (pinNum == 6) {
    //        potar_12 = ofMap(ard.getAnalog(pinNum), 0, 1024, 0, 2);
    //        //potar_12 = roundf(potar_12 * 10) / 10;
    //        opBC_ch3 = potar_12;
    //    }
    //    if (pinNum == 7) {
    //        potar_14 = ofMap(ard.getAnalog(pinNum), 0, 1024, 0, 1);
    //        //potar_14 = roundf(potar_14 * 10) / 10;
    //        // extension_3channel to add
    //    }
}
