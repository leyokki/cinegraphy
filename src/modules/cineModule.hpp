//
//  cineModule.hpp
//  midiMapper
//
//  Created by lky on 21/12/2019.
//

#ifndef cineModule_hpp
#define cineModule_hpp

#include "ofMain.h"
#include <stdio.h>

class cineModule: public ofBaseApp {
public:
    cineModule();
    virtual ~cineModule();
    virtual void reset() = 0;

    virtual void update() = 0;
    virtual void update(const double&, const int&) = 0;
    virtual void update_still() = 0;
    virtual void update_still(const double&, const int&) = 0;
    
    virtual void draw(double scale = 1) = 0;
    virtual void draw(float x, float y) = 0;
    
    virtual void increment_image() = 0;
    virtual void decrement_image() = 0;

    virtual void update_speed(const double&) = 0;
    virtual void update_frame(const double&) = 0;
    
    virtual void update_image_selected(const int&) = 0;
    virtual void update_speed_or_frame(const double&) = 0;
    
    virtual int get_image_selected() = 0;
    virtual double get_speed_or_frame() = 0;
    
    virtual ofPixels get_pixels() = 0;
    
    virtual ofTexture * getTexture() = 0;
    
    string get_type();
    int get_module_width();
    int get_module_height();
    
protected:
    string type_name;
    int m_module_width, m_module_height;
};

#endif /* cineModule_hpp */
