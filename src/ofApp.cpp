/*
 * Lky
 *
 * Feel free to modify, share, or whatever =)
 *
 *
 */

#include "ofApp.h"
#include <cstring>
#include <cctype>

#include <chrono>
//-------------------  -------------------------------------------

void ofApp::setup() {
    
	ofSetVerticalSync(true);
	ofBackground(0);
    ofSetFrameRate(60);
    ofSetCircleResolution(200);
    ofSetLogLevel(OF_LOG_SILENT); //  ofSetLogLevel(OF_LOG_VERBOSE);
    
    m_console.setup("ofWood_config_2ch_files");
    m_console_pointer = &m_console;
    
    midi_console.setup(m_console_pointer, "config_2ch_midi");
    m_recorder.init("_export/", m_console.get_global_w(), m_console.get_global_h());
    
    record_fbo = cineUtils::utils_init_fbo(m_console.get_global_w(), m_console.get_global_h());
    
	init_key_preset();
    load_key_preset("lky_keyset");
    cout << "[setup] Everything is loaded." << "\n";
}


//--------------------------------------------------------------
void ofApp::update() {    
    m_console.update();
    record_fbo = m_console.get_fbo();
}

//--------------------------------------------------------------
void ofApp::draw() {
    ofScale(0.5);
    show_frame_rate();
    
    record_fbo.draw(0,0);

    m_recorder.update(record_fbo);
}

//--------------------------------------------------------------
void ofApp::exit() {
    m_recorder.exit();
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {
}

//--------------------------------------------------------------
void ofApp::mouseReleased() {
}

void ofApp::show_frame_rate() {
    string windowName = ofToString(ofGetFrameRate());
    ofSetWindowTitle(windowName + "frame ");
}
