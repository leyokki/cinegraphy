#pragma once

#include "ofMain.h"
#include "ofxXmlSettings.h"

#include "cineConsole.hpp"

#include "modules/main/cineChannel.hpp"
#include "modules/recorder/cineRecorder.hpp"

#include "modules/interface/cineGui.hpp"
#include "modules/interface/cineDuino.hpp"
#include "modules/interface/cineMidi/cineMidi.hpp"

#include "utils/utilities.hpp"

class ofApp : public ofBaseApp {
    
public:

	void setup();
    void load_xml_presets(string xml_path);
    
    void update();

    void draw();
	void exit();

	void keyPressed(int key);
	void keyReleased(int key);

	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased();

    void show_frame_rate();
    
    void init_key_preset();
    void load_key_preset(string);
        
    void write_key_preset(string name);

    void apply_keymapped_function(int);

private:
    
    cineConsole m_console; //
    cineMidi midi_console; //
    cineRecorder m_recorder; //
    cineConsole *m_console_pointer;
    
    ofxXmlSettings m_xml_presets;
    ofFbo record_fbo;
//    *ofFbo record_fbo_pointer;

    static const int keystrokes_number = 127;
    static const int keymap_variables = 3;
    
    string keyboard_keymap[keystrokes_number][keymap_variables];
};
