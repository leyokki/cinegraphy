//
//  utils.cpp
//  midiMapper
//
//  Created by lky on 15/10/2019.
//

#include "utilities.hpp"

void cineUtils::default_background(const int& width, const int& height) {
    ofSetColor(0); // old:: Color==10
    ofDrawRectangle(0, 0, width, height);
    ofSetColor(255);
}

ofFbo cineUtils::utils_init_fbo(const int& fbo_w, const int& fbo_h){
    ofFbo fbo;
    fbo.allocate(fbo_w,fbo_h, GL_RGB);
    fbo.begin();
    ofClear(0, 0, 0);
    fbo.end();
    
    return fbo;
}

vector<string> cineUtils::list_paths(const string& dir){
    ofDirectory actualDir;
    actualDir.allowExt("mov");
    actualDir.open(dir);
    actualDir.listDir();
    
    vector<string> paths;
    
    for (int i = 0; i < actualDir.size(); ++i) {
        string path = actualDir.getPath(i);
        ofLog(OF_LOG_VERBOSE,path);
        paths.push_back(path); // <!> cannot check if src is correct, should be done later. <!>
    }
    std::sort(paths.begin(),paths.end());
    ofLog() << "Paths loaded" << std::endl;
    
    return paths;
}


// TO REMOVE LATER

std::tuple<int,int> getAverageMultipliers(int value) {
    int a = 1;
    int tmp = a;
    int b = value;
    
    while (a < b) {
        tmp++;
        if ((value % a) == 0) {
            a = tmp;
            b = value/a;
        }
    };
    return make_tuple(a,b);
}

std::tuple<vector<int>, vector<int>> setupSplit(ofImage src, bool random, int arraySize, int screen_w, int screen_h) {
    int w = src.getWidth();
    int h = src.getHeight();
    
    vector<int> serieX, serieY;
    int row, col;
    //    int max = (w >= h) ? 1 : 0; //not used
    
    // split in a square: 3 * 3
    // might have to change it later : 4 * 3 [12]
    
    if (w >= h)
    {
        tie(row,col) = getAverageMultipliers(arraySize);
    } else {
        tie(col,row) = getAverageMultipliers(arraySize);
    }
    
    if (random) { // define random points
        for (int i = 0; i < arraySize; ++i) {
            serieX.push_back(-rand() % (w - screen_w / 2));
            serieY.push_back(-rand() % (h - screen_h / 2));
        }
    }
    else {
        int i = 0; int j = 0;
        for (int a = 0; a < arraySize; ++a) {
            if (j >= col) {
                j = 0;
                ++i;
            }
            // note: negative value, it is a drawing position.
            serieX.push_back(-i * w / row);
            serieY.push_back(-j * h / col);
            ++j;
        }
    }
    
#ifdef VERBOSE
    ofLog() << "[verbose] Splits::" << std::endl;
    for (int a = 0; a < arraySize; ++a) {
        ofLog() << "(" << serieX[a] << "," << serieY[a] << ") ; ";
    }
    ofLog() << "[verbose] setupUp init" << std::endl;
#endif
    
    return make_tuple(serieX, serieY);
}

double reverse_value_every_other(double value, int count) { // to be replaced soon
    return abs(value - (count % 2));
}

