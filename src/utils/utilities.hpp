//
//  utils.hpp
//  midiMapper
//
//  Created by lky on 15/10/2019.
//

#ifndef utilities_hpp
#define utilities_hpp

#pragma once

#include <stdio.h>
#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxXmlSettings.h"

std::tuple<vector<int>, vector<int>> setupSplit(ofImage src, bool random = false, int arraySize = 12, int screen_w = 1440, int screen_h = 1080);

double reverse_value_every_other(double value, int count);

class cineUtils {
public:
    static void default_background(const int&, const int&);
    
    static ofFbo utils_init_fbo(const int& fbo_w, const int& fbo_h);
    
    static vector<string> list_paths(const string& dir);
};

class cinegraphy {
public:
    // unused for now
    enum blendmode {
        blend, // (a + b) / 2
        multiply, // (a * b)
        screen, // 1 - (1 - a) * (1 - b)
        darken, // min(a, b)
        lighten, // max(a, b)
        difference, // abs(a - b)
        exclusion, // a + b - 2 * a * b
        overlay, // a < .5 ? (2 * a * b) : (1 - 2 * (1 - a) * (1 - b))
        hard_light, // b < .5 ? (2 * a * b) : (1 - 2 * (1 - a) * (1 - b))
        softlight, // b < .5 ? (2 * a * b + a * a * (1 - 2 * b)) : (sqrt(a) * (2 * b - 1) + (2 * a) * (1 - b))
        dodge, // a / (1 - b)
        burn //  1 - (1 - a) / b
    };
    
    static string get_blend_name(const int& name) {
        switch(name){
            case 0:
                return "blend";
                break;
            case 1:
                return "multiply";
                break;
            case 2:
                return "screen";
                break;
            case 3:
                return "darken";
                break;
            case 4:
                return "lighten";
                break;
            case 5:
                return "difference";
                break;
            case 6:
                return "exclusion";
                break;
            case 7:
                return "overlay";
                break;
            case 8:
                return "hard_light";
                break;
            case 9:
                return "soft_light";
                break;
            case 10:
                return "dodge";
                break;
            case 11:
                return "burn";
                break;
            default:
                return "blend";
                break;
        }
    }
};

#endif /* utils_hpp */
