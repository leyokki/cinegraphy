//
//  cine_functions_enum.cpp
//  midiMapper
//
//  Created by lky on 19/11/2020.
//

#include "utils/cine_functions_enum.hpp"

cine_functions check_cine_function(const std::string& input) {
    // KEYS
    if( input == "none" ) return none;
    if( input == "set_x_pos" ) return set_x_pos;
    if( input == "set_y_pos" ) return set_y_pos;
    if( input == "set_scale" ) return set_scale;
    if( input == "set_fade" ) return set_fade;

    if( input == "set_blend_modes") return set_blend_modes;
    if( input == "increment_blend_mode") return increment_blend_mode;
    if( input == "decrement_blend_mode") return decrement_blend_mode;
    if( input == "activate_channel" ) return activate_channel;
    if( input == "update_channel_image" ) return update_channel_image;
    
    // CONTROLS
    if( input == "set_contrast" ) return set_contrast;
    if( input == "set_saturation" ) return set_saturation;
    if( input == "set_brightness" ) return set_brightness;
    if( input == "update_channel_fade" ) return update_channel_fade;
    if( input == "update_channel_speed_or_frame" ) return update_channel_speed_or_frame;
    
    return none;
}
