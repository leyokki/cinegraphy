//
//  cine_functions_enum.h
//  midiMapper
//
//  Created by lky on 19/11/2020.
//

#ifndef cine_functions_enum_h
#define cine_functions_enum_h

#include <stdio.h>
#include <string>

enum cine_functions {
    none,
    set_x_pos,
    set_y_pos,
    set_scale,
    set_fade,
    set_contrast,
    set_saturation,
    set_brightness,
    set_blend_modes,
    increment_blend_mode,
    decrement_blend_mode,
    activate_channel,
    update_channel_image,
    update_channel_speed_or_frame,
    update_channel_fade,
};

cine_functions check_cine_function(const std::string&);

#endif /* cine_functions_enum_h */
