//
//  cineConsole.hpp
//  midiMapper
//
//  Created by lky on 28/01/2020.
//

#ifndef cineConsole_hpp
#define cineConsole_hpp

#pragma once

#include "ofMain.h"
#include <stdio.h>
#include "ofxXmlSettings.h"

#include "modules/main/cineChannel.hpp"

class cineConsole: public ofBaseApp {
    
public:
    
    cineConsole();
    cineConsole(string name);
    cineConsole(cineConsole const& other);
    ~cineConsole();

    void setup(string path);
    void add_channel(string type, int w, int h, string path);
    void load_xml(string);

    void update();
    
    void draw();
    void draw(float x, float y, float scale);
    void draw_three_screens(ofFbo reference,int w, int h);
    
    ofFbo get_fbo();
        
    void set_x_pos(double value);
    void set_y_pos(double value);
    void set_scale(double value);
    
    void set_fade(double value);
    void set_thresh(double value);
    void switch_thresh();
    
    void set_brightness(double value);
    void set_contrast(double value);
    void set_saturation(double value);

    void get_blend_mode_channel(int channel);

    void activate_channel(int channel, bool value);
    void update_channel_image(const int channel, const int value);
    void update_channel_speed_or_frame(int channel, double value);
    void update_channel_fade(int channel, double fade);
    void update_channel_reversed_fade(int channel, double fade);

    void reset_channel(int channel);
    void reset_channel_image(int channel);
    void increment_channel_image(int channel);
    void decrement_channel_image(int channel);
    
    void set_second_sat(float val);
    void set_second_bright(float val);
    void set_second_cont(float val);
    
    void switch_channel_mirror(int channel, int axis_x);
    
    double get_global_w();
    double get_global_h();

    ofFbo mix_multiple_channels();
    
    void get_lines();

    int tmp_threshMod;
    vector<ofPolyline> lines;
    
    void increment_blend_mode();
    void decrement_blend_mode();
   
    void set_blend_modes(int channel, int value);
    void increment_blend_modes(int channel);
    void decrement_blend_modes(int channel);

    void increment_local_h(int channel);
    void decrement_local_h(int channel);
    void increment_local_w(int channel);
    void decrement_local_w(int channel);
    
    void switch_channel();
    

private:
    
    string m_name;
    
    vector<cineChannel> m_channels;
    vector<int> m_blend_modes;
    
    double m_scale;
    double m_global_w, m_global_h;
    double m_x_pos, m_y_pos;
    int check_motion;
    
    float m_grain_amount;
    float m_grain_size;
    float m_is_grain_colored;
    
    float m_saturation;
    float m_contrast;
    float m_brightness;
    
    float m_fade_value;

    int m_blend_mode;
    float m_thresh;

    // will change to blendmode
    int m_difference_mode_activated = 0;
        
    float second_contrast, second_saturation, second_brightness;
    
    int m_actual_channel;
    
    ofTrueTypeFont log_font;
};

#endif /* cineConsole_hpp */
