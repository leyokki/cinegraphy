/*
 * Lky
 *
 * Feel free to modify, share, or do whatever you like with 
 * my code.
 *
 *
 */

#include "ofApp.h"
#include "utils/cine_functions_enum.hpp"

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
    try {
        (key < keystrokes_number) ? apply_keymapped_function(key) : void();
    }
    catch (...) {
        ofLog() << "KEY SHORTCUT:: Non recognized function or non valid variables" << endl;
    }
}

void ofApp::keyReleased(int key) {
    switch (key) {
        case ' ':
            m_recorder.record();
            break;
    }
}

void ofApp::load_key_preset(string path) {
    ofxXmlSettings xml_midi_key_presets;

    if (xml_midi_key_presets.loadFile("_xml/keymap/"+path+".xml")) {
        ofLog() << "xml loaded!" << '\n';
    } else {
        ofLog() << "unable to load " + path + " check _xml/ folder" << '\n';
    }

    int number_of_keys = xml_midi_key_presets.getNumTags("KEY");

    for (int i = 0; i < number_of_keys; ++i) {
        
        xml_midi_key_presets.pushTag("KEY",i);
        int key = xml_midi_key_presets.getValue("KEY_VALUE", 0);
        string xml_function = xml_midi_key_presets.getValue("FUNCTION", "none");
        string xml_channel = xml_midi_key_presets.getValue("CHANNEL", "undefined");
        string xml_value = xml_midi_key_presets.getValue("VALUE", "undefined");
        xml_midi_key_presets.popTag();

        if (key != 0) {
            keyboard_keymap[key][0] = xml_function;
            keyboard_keymap[key][1] = xml_channel;
            keyboard_keymap[key][2] = xml_value;

        }
    }
}

void ofApp::apply_keymapped_function(int value) {
    switch(check_cine_function(keyboard_keymap[value][0])) {
        case none:
            break;
        case set_blend_modes:
            m_console.set_blend_modes(std::stoi(keyboard_keymap[value][1]),std::stoi(keyboard_keymap[value][2]));
            break;
        case update_channel_image:
            m_console.update_channel_image(std::stoi(keyboard_keymap[value][1]),std::stoi(keyboard_keymap[value][2]));
            break;
        case increment_blend_mode:
            m_console.increment_blend_mode();
            break;
        case decrement_blend_mode:
            m_console.decrement_blend_mode();
            break;
        default:
            break;
    }
}


void ofApp::init_key_preset(){
    for (int i = 0; i < keystrokes_number; ++i) {
        keyboard_keymap[i][0] = "none";
        keyboard_keymap[i][1] = "undefined value";
        keyboard_keymap[i][2] = "undefined value";
    }
}
