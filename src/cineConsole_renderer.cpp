//
//  cineConsole_renderer.cpp
//  midiMapper
//
//  Created by lky on 20/11/2020.
//

#include <stdio.h>
#include "cineConsole.hpp"


ofFbo blending_pass(ofFbo fbo_1, ofFbo fbo_2, float fade_1, float fade_2, string blendmode) {
    ofShader blending_shader;
    ofFbo blended_fbo;
    
    blending_shader.load("_shaders/blendingMode/" + blendmode);
    
    blended_fbo.allocate(fbo_1.getWidth(),fbo_1.getHeight());
    blended_fbo.begin();

    blending_shader.begin();
    ofPushMatrix();

    blending_shader.setUniformTexture("ch1", fbo_1.getTexture(), 0);
    blending_shader.setUniformTexture("ch2", fbo_2.getTexture(), 1);
    
    blending_shader.setUniform1f("fade_ch1", fade_1);
    blending_shader.setUniform1f("fade_ch2", fade_2);

    fbo_1.draw(0,0);
    
    ofPopMatrix();

    blending_shader.end();
    blended_fbo.end();
    
    return blended_fbo;
}

ofFbo CSB_Pass(ofFbo fbo_1, const float& con, const float& sat, const float& bri) {
    ofShader CSB_shader;
    ofFbo new_fbo;
    CSB_shader.load("_shaders/utils_shader/CSB");
    
    new_fbo.allocate(fbo_1.getWidth(),fbo_1.getHeight());
    
    new_fbo.begin();
    CSB_shader.begin();
    
    ofPushMatrix();
    
    CSB_shader.setUniformTexture("ch1", fbo_1.getTexture(), 0);
    
    CSB_shader.setUniform1f("saturation", con);
    CSB_shader.setUniform1f("contrast", sat);
    CSB_shader.setUniform1f("brightness", bri);

    fbo_1.draw(0,0);
    
    ofPopMatrix();
    CSB_shader.end();
    new_fbo.end();
    
    return new_fbo;
}

ofFbo cineConsole::mix_multiple_channels() {//vector<cineChannel> *channels) {
    ofFbo blended_fbo;
//    float last_fade = m_channels[0].get_fade();
    // m_fade_value
    blended_fbo = cineUtils::utils_init_fbo(m_channels[0].get_width(), m_channels[0].get_heigth());
//    blended_fbo = blending_pass(blended_fbo, m_channels[0].get_local_fbo(), 1, m_channels[0].get_fade(), cinegraphy::get_blend_name(m_blend_mode));
    
    int last_active = 0;
    
    for (int i = 0; i < m_channels.size() - 1; ++i) {
        blended_fbo = m_channels[i].is_active() ? blending_pass(blended_fbo, m_channels[i].get_local_fbo(), m_channels[last_active].get_fade(), m_channels[i].get_fade(), cinegraphy::get_blend_name(m_blend_mode)) : blended_fbo;
        last_active = i;
    }
    
    return CSB_Pass(blended_fbo, m_contrast, m_saturation, m_brightness);;
}
